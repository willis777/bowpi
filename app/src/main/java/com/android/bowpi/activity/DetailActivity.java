package com.android.bowpi.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.bowpi.util.ConstantsTask;
import com.android.bowpi.helper.FirebaseDatabaseHelper;
import com.android.bowpi.R;
import com.android.bowpi.model.Task;
import java.util.Date;
import java.util.List;

public class DetailActivity extends AppCompatActivity {

    private TextView tName, tDescription, tState, tCreationDate, tModificationDate;
    private ImageView imvDone, imvCancel, imvRemove;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        try{

            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_detail);
            setTheme(R.style.Theme_Bowpi);

            tName = (TextView) findViewById(R.id.tv_item_detail_name);
            tDescription = (TextView) findViewById(R.id.tv_item_detail_description);
            tState = (TextView) findViewById(R.id.tv_item_detail_state);
            tCreationDate = (TextView) findViewById(R.id.tv_item_detail_creation_date);
            tModificationDate = (TextView) findViewById(R.id.tv_item_detail_modification_date);

            imvDone = (ImageView) findViewById(R.id.imv_done);
            imvCancel = (ImageView) findViewById(R.id.imv_cancel);
            imvRemove = (ImageView) findViewById(R.id.imv_remove);

            if(getSupportActionBar() != null){
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            Intent intent = getIntent();
            Task task = intent.getParcelableExtra("task");

            tName.setText(task.getName());
            tDescription.setText(task.getDescription());

            switch (task.getState()){

                case ConstantsTask.PENDING:
                    tState.setBackgroundResource(R.color.gold);
                    break;

                case ConstantsTask.DONE:
                    tState.setBackgroundResource(R.color.teal_200);
                    break;

                case ConstantsTask.CANCELED:
                    tState.setBackgroundResource(R.color.red);
                    break;
            }

            tState.setText(task.getState());
            tCreationDate.setText(new Date(task.getCreationDate()).toString());
            tModificationDate.setText(new Date(task.getModificationDate()).toString());

            imvDone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    task.setState(ConstantsTask.DONE);

                    new FirebaseDatabaseHelper().updateTask(task, new FirebaseDatabaseHelper.DataEvent() {
                        @Override
                        public void dataLoaded(List<Task> tasks) {

                        }

                        @Override
                        public void dataAdded() {

                        }

                        @Override
                        public void dataUpdated() {

                            Toast.makeText(DetailActivity.this, "Task was Completed", Toast.LENGTH_LONG).show();

                            finish();
                        }

                        @Override
                        public void dataDeleted() {



                        }
                    });


                }
            });




            imvCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    task.setState(ConstantsTask.CANCELED);

                    new FirebaseDatabaseHelper().updateTask(task, new FirebaseDatabaseHelper.DataEvent() {
                        @Override
                        public void dataLoaded(List<Task> tasks) {

                        }

                        @Override
                        public void dataAdded() {

                        }

                        @Override
                        public void dataUpdated() {

                            Toast.makeText(DetailActivity.this, "Task was Canceled", Toast.LENGTH_LONG).show();

                            finish();

                        }

                        @Override
                        public void dataDeleted() {



                        }
                    });
                }
            });

            imvRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    new FirebaseDatabaseHelper().deleteTask(task, new FirebaseDatabaseHelper.DataEvent() {
                        @Override
                        public void dataLoaded(List<Task> tasks) {

                        }

                        @Override
                        public void dataAdded() {

                        }

                        @Override
                        public void dataUpdated() {

                        }

                        @Override
                        public void dataDeleted() {

                            Toast.makeText(DetailActivity.this, "Task was Removed", Toast.LENGTH_LONG).show();

                            finish();

                        }
                    });

                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        try{
            if (item.getItemId() == android.R.id.home) {
                finish();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return super.onOptionsItemSelected(item);
    }

}
