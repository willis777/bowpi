package com.android.bowpi.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;
import com.android.bowpi.util.ConstantsTask;
import com.android.bowpi.helper.FirebaseDatabaseHelper;
import com.android.bowpi.R;
import com.android.bowpi.model.Task;
import java.util.List;

public class AddActivity extends AppCompatActivity {

    private EditText eName, eDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setTheme(R.style.Theme_Bowpi);
            setContentView(R.layout.activity_add);
            setTitle(R.string.add_new);
            eName = (EditText) findViewById(R.id.eName);
            eDescription = (EditText) findViewById(R.id.eDescription);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        try {
            int id = item.getItemId();
            if (id == R.id.action_save) {
                saveTask();
                return true;
            }
            if (item.getItemId() == android.R.id.home) {
                finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.onOptionsItemSelected(item);
    }

    public void saveTask() {

        try {

            String name = eName.getText().toString();
            String description = eDescription.getText().toString();

            if (name.isEmpty()) {
                eName.setError(getResources().getString(R.string.fill_name));
            }

            if (description.isEmpty()) {
                eDescription.setError(getResources().getString(R.string.description));
            }

            if (!name.isEmpty() && !description.isEmpty()) {

                Task task = new Task(name, description, ConstantsTask.PENDING);

                new FirebaseDatabaseHelper().addTask(task, new FirebaseDatabaseHelper.DataEvent() {

                    @Override
                    public void dataLoaded(List<Task> tasks) {

                    }

                    @Override
                    public void dataAdded() {
                        Toast.makeText(AddActivity.this, "The Task was added ", Toast.LENGTH_LONG).show();
                        finish();
                    }

                    @Override
                    public void dataUpdated() {

                    }

                    @Override
                    public void dataDeleted() {

                    }
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}