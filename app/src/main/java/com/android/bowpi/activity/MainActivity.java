package com.android.bowpi.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import com.android.bowpi.util.ConstantsTask;
import com.android.bowpi.R;
import com.android.bowpi.helper.RecyclerViewHelper;
import com.android.bowpi.model.Task;
import com.android.bowpi.helper.FirebaseDatabaseHelper;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rvTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        try {
            setTheme(R.style.Theme_Bowpi_Splash);
            super.onCreate(savedInstanceState);
            Thread.sleep(500);
            setTheme(R.style.Theme_Bowpi);
            setContentView(R.layout.activity_main);

            rvTask = (RecyclerView) findViewById(R.id.rv_task);

            new FirebaseDatabaseHelper().getTask(new FirebaseDatabaseHelper.DataEvent() {
                @Override
                public void dataLoaded(List<Task> tasks) {
                    new RecyclerViewHelper().setConfig(MainActivity.this,rvTask,tasks);
                }

                @Override
                public void dataAdded() {

                }

                @Override
                public void dataUpdated() {

                }

                @Override
                public void dataDeleted() {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_add) {
            startActivity(new Intent(this, AddActivity.class));
            return true;
        }

        if (id == R.id.pending) {


            new FirebaseDatabaseHelper().getTaskByState(ConstantsTask.PENDING, new FirebaseDatabaseHelper.DataEvent() {
                @Override
                public void dataLoaded(List<Task> tasks) {
                    new RecyclerViewHelper().setConfig(MainActivity.this,rvTask,tasks);
                }

                @Override
                public void dataAdded() {

                }

                @Override
                public void dataUpdated() {

                }

                @Override
                public void dataDeleted() {

                }
            });

            return true;
        }

        if (id == R.id.done) {

            new FirebaseDatabaseHelper().getTaskByState(ConstantsTask.DONE, new FirebaseDatabaseHelper.DataEvent() {
                @Override
                public void dataLoaded(List<Task> tasks) {
                    new RecyclerViewHelper().setConfig(MainActivity.this,rvTask,tasks);
                }

                @Override
                public void dataAdded() {

                }

                @Override
                public void dataUpdated() {

                }

                @Override
                public void dataDeleted() {

                }
            });

            return true;
        }

        if (id == R.id.canceling) {
            new FirebaseDatabaseHelper().getTaskByState(ConstantsTask.CANCELED, new FirebaseDatabaseHelper.DataEvent() {
                @Override
                public void dataLoaded(List<Task> tasks) {
                    new RecyclerViewHelper().setConfig(MainActivity.this,rvTask,tasks);
                }

                @Override
                public void dataAdded() {

                }

                @Override
                public void dataUpdated() {

                }

                @Override
                public void dataDeleted() {

                }
            });

            return true;
        }


        return super.onOptionsItemSelected(item);
    }
}