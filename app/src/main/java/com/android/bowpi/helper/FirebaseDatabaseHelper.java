package com.android.bowpi.helper;

import androidx.annotation.NonNull;
import com.android.bowpi.model.Task;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.List;

public class FirebaseDatabaseHelper {

    private FirebaseDatabase mDatabase;
    private DatabaseReference mReferenceTasks;
    private List<Task> tasks = new ArrayList<>();

    public interface DataEvent {
        void dataLoaded (List<Task> tasks);
        void dataAdded();
        void dataUpdated();
        void dataDeleted();
    }

    public FirebaseDatabaseHelper(){

        try{

            mDatabase = FirebaseDatabase.getInstance();
            mReferenceTasks = mDatabase.getReference("task");

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void getTaskByState(String state, final DataEvent dataEvent) {

        try{

            mReferenceTasks.orderByChild("state").equalTo(state).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {

                    tasks.clear();

                    for(DataSnapshot key : snapshot.getChildren()){

                        Task task = key.getValue(Task.class);
                        task.setKey(key.getKey());
                        tasks.add(task);
                    }

                    dataEvent.dataLoaded(tasks);

                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void getTask(final DataEvent dataEvent) {

        try{

            mReferenceTasks.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {

                    tasks.clear();

                    List<String> keys = new ArrayList<>();

                    for(DataSnapshot key : snapshot.getChildren()){

                        Task task = key.getValue(Task.class);
                        task.setKey(key.getKey());
                        tasks.add(task);
                    }

                    dataEvent.dataLoaded(tasks);

                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void addTask (Task task, final DataEvent dataEvent) {

        try{

            String key = mReferenceTasks.push().getKey();

            mReferenceTasks.child(key).setValue(task).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    dataEvent.dataAdded();
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void updateTask (Task task, final DataEvent dataEvent ){

        try{

            mReferenceTasks.child(task.getKey()).setValue(task)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            dataEvent.dataUpdated();
                        }
                    });

        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void deleteTask (Task task, final DataEvent dataEvent ){

        try{

            mReferenceTasks.child(task.getKey()).setValue(null)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            dataEvent.dataDeleted();
                        }
                    });

        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
