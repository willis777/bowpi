package com.android.bowpi.helper;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.android.bowpi.adapter.TaskAdapter;
import com.android.bowpi.model.Task;
import java.util.List;

public class RecyclerViewHelper {

    private Context mContext;
    private TaskAdapter mTaskAdapter;

    public void setConfig (Context context, RecyclerView recyclerView, List<Task> tasks){

        try{

            mContext = context;
            mTaskAdapter = new TaskAdapter(mContext,tasks);
            recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
            recyclerView.setAdapter(mTaskAdapter);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

}
