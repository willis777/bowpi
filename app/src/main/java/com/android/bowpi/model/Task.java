package com.android.bowpi.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Date;
import java.util.Objects;

public class Task implements Parcelable {

    private String key;
    private String name;
    private String description;
    private String state;
    private Long creationDate;
    private Long modificationDate;

    public Task() {
        this.key = "";
        this.name = "";
        this.description = "";
        this.state = "";
        this.creationDate = new Date().getTime();
        this.modificationDate = new Date().getTime();
    }

    public Task(Parcel parcel){
        this.key = parcel.readString();
        this.name = parcel.readString();
        this.description = parcel.readString();
        this.state = parcel.readString();
        this.creationDate = parcel.readLong();
        this.modificationDate = parcel.readLong();
    }

    public Task(String name, String description, String state) {
        this.name = name;
        this.description = description;
        this.state = state;
    }


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Long creationDate) {
        this.creationDate = creationDate;
    }

    public Long getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Long modificationDate) {
        this.modificationDate = modificationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return key.equals(task.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(key);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(state);
        dest.writeLong(creationDate);
        dest.writeLong(modificationDate);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator<Task>(){

        @Override
        public Task createFromParcel(Parcel source) {
            return new Task(source);
        }

        @Override
        public Task[] newArray(int size) {
            return new Task[size];
        }
    };
}
