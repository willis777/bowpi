package com.android.bowpi.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.android.bowpi.util.ConstantsTask;
import com.android.bowpi.R;
import com.android.bowpi.activity.DetailActivity;
import com.android.bowpi.model.Task;
import java.util.List;

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.ViewHolder> {

    private List<Task> taskList;
    private Context context;

    public TaskAdapter(Context context, List<Task> taskList) {
        this.context = context;
        this.taskList = taskList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        ViewHolder viewHolder = null;

        try{

            Context context = parent.getContext();

            LayoutInflater inflater = LayoutInflater.from(context);

            View view = inflater.inflate(R.layout.item_task, parent, false);

            viewHolder = new ViewHolder(view);


        }catch (Exception e){
            e.printStackTrace();
        }
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        try{

            Task task = taskList.get(position);

            TextView tvName = holder.name;
            tvName.setText(task.getName());
            TextView tvDescription = holder.description;
            tvDescription.setText(task.getDescription());
            TextView tvState = holder.state;

            switch (task.getState()){

                case ConstantsTask.PENDING:
                    tvState.setBackgroundResource(R.color.gold);
                    break;

                case ConstantsTask.DONE:
                    tvState.setBackgroundResource(R.color.teal_200);
                    break;

                case ConstantsTask.CANCELED:
                    tvState.setBackgroundResource(R.color.red);
                    break;
            }

            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(context, DetailActivity.class);
                    intent.putExtra("task",task);
                    context.startActivity(intent);

                }
            });

            tvState.setText(task.getState());

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return taskList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private CardView cardView;
        public TextView name;
        public TextView description;
        public TextView state;

        public ViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.card_view);
            name = (TextView) itemView.findViewById(R.id.tv_item_name);
            description = (TextView) itemView.findViewById(R.id.tv_item_description);
            state = (TextView) itemView.findViewById(R.id.tv_item_state);

        }
    }

}



